from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class ChistesHandler(ContentHandler):
    """
    Clase para manejar chistes malos
    """

    def __init__(self,numero=0,valor=0):
        """
        Constructor. Inicializamos las variables
        """
        self.calificacion = ""
        self.pregunta = ""
        self.inPregunta = False
        self.respuesta = ""
        self.inRespuesta = False
        self.count = numero
        self.orden = valor

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """
        if name == 'chiste':
            # De esta manera tomamos los valores de los atributos

            self.calificacion = attrs.get('calificacion', "")

            self.count = self.count + 1
            print("Chiste número: " + str(self.count) + " (" + self.calificacion + ")")


        elif name == 'pregunta':

            self.pregunta = ""
            self.inPregunta = True

        elif name == 'respuesta':

            self.respuesta = ""
            self.inRespuesta = True

    def endElement(self, name):
        """
        Método que se llama al cerrar una etiqueta
        """
        if name == 'pregunta':
            self.inPregunta = False
        elif name == 'respuesta':
            self.inRespuesta = False

    def characters(self, char):
        """
        Método para tomar contenido de la etiqueta
        """

        if self.inPregunta:
            self.pregunta += char
            if self.orden == 0:
                self.orden = self.orden + 1
            elif self.orden == 1:
                print("Pregunta: "+self.pregunta)
                print("Respuesta: "+self.respuesta+"\n")
                self.orden = self.orden-1
        elif self.inRespuesta:
            self.respuesta += char
            if self.orden == 0:
                self.orden = self.orden + 1
            elif self.orden == 1:
                print("Pregunta: "+self.pregunta)
                print("Respuesta: "+self.respuesta+"\n")
                self.orden=self.orden-1


def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = ChistesHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('chistes.xml'))


if __name__ == "__main__":
    main()