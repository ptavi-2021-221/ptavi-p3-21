from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SMILHandler(ContentHandler):

    def __init__(self):
        self.dicc =  {"root-layout" : ["width", "height", "background-color"],
                            "region" : ["id", "top", "bottom", "left", "right"],
                            "img" : ["src", "region", "begin", "dur"],
                            "audio" : ["src", "begin", "dur"],
                            "textstream" : ["src", "region", "fill"]
                            }
        self.list = []

    def startElement(self, tag, atributo):
        if tag in self.dicc:
            dicc2={}
            for attr in self.dicc[tag]:
                dicc2[attr] = atributo.get(attr,"")
            self.list.append(['attrs',dicc2])

    def get_tags(self):
        return self.list


def main():
    """Programa principal"""
    parser = make_parser()
    cHandler = SMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    lista = cHandler.get_tags()
    print(lista)


if __name__ == "__main__":
    main()
